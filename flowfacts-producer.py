import requests
import json
import hmac
import hashlib
from datetime import timedelta

from tgvfunctions import tgvfunctions
from projectsecrets import FFAPIKEY, HMAC_SHA_KEY, CLIENT_ID, NODE_ID_SENSOR1, NODE_ID_SENSOR2 
project_description = "Flowsand (zand met sponswerking) neemt snel water op en geeft dat langzaam af aan de ondergrond. Het flowsand bevindt zich in de grastegels en daaronder liggende straat laag. Daarin wordt, net als in een spons, zoveel mogelijk water vastgehouden. Overtollig water zakt verder in het onderliggende Aquaflowsysteem. De watervoorraad zal vanuit het flowsand verdampen, waardoor de temperatuur op straat wordt verlaagd en het “heat island” effect in steden wordt gereduceerd."

# Determine time interval  (begintime is determined later from timefiles)
endtime = tgvfunctions.roundTime(roundTo=60).timestamp()
# identify sensors
sensors = [NODE_ID_SENSOR1, NODE_ID_SENSOR2]

# Create producer
producer = tgvfunctions.makeproducer('flowfacts-producer')

for sensor in sensors:
# 1) Read out sensor info
    # Define headers and parameters
    headers = {"FFAPIKEY": FFAPIKEY}
    params = {"client_id": CLIENT_ID, "node_id": sensor}
    r = requests.get(
        "https://api.flowfacts.nl/v1/nodes", headers = headers, params=params
    )

    # Error: incorrect api request
    if not r.status_code == 200:
        print('Error: incorrect api request, HTTP error code',r.status_code)
        exit()

    # check hash code to verify message integrity. From the documentation: HMAC(SHA-256, <dt_value>-<data_value>, <secret key>)
    byte_key = HMAC_SHA_KEY.encode('utf-8')
    msg = json.dumps(r.json()["dt"]) + json.dumps(r.json()["data"], separators=(",",":"))
    h = hmac.new( byte_key, msg.encode('utf-8'), hashlib.sha256 ).hexdigest()
    
    # Error: incorrect checksum -> data are incorrect
    if h != r.json()["checksum"]:
        print('Error: checksums do not match - data should be discarded')
        exit()

    node_deveui = r.json()["data"][0]["deveui"]
    location_description =  r.json()["data"][0]["description"] + " Voor meer uitgebreide informatie zie tekening met locaties op databank waterstraat (vraag bij projectmanager)."
    latitude = float(r.json()["data"][0]["latitude"])
    longitude = float(r.json()["data"][0]["longitude"])
    #altitude N.B. Altitude in meters above the mean sea level

    # The offset of the electrical conductivity sensor (μS/cm):
    ec_offset = float(r.json()["data"][0]["ec_offset"])
    # The offset of the temperature sensor (°C):
    temp_offset = float(r.json()["data"][0]["temp_offset"])
    # The offset of the volumetric water content sensor (%VWC m3/m3):
    vwc_offset = float(r.json()["data"][0]["vwc_offset"])

    r.close()

    # 2) Read out sensor measurements

    # Define headers and parameters
    headers = {"FFAPIKEY": FFAPIKEY}
    # begintime is used in a closed time interval, so +1 second should be added 
    # to prevent producing the same value twice.
    begintime = tgvfunctions.timestampread('timefile'+str(sensor),3600) + 1 # one data point every hour
    params = {"from": begintime, "to": endtime, "client_id": CLIENT_ID, "node_id": sensor, "node_deveui": node_deveui}
    r = requests.get(
        "https://api.flowfacts.nl/v1/measurements", headers = headers, params=params
    )
    
    # Error: incorrect api request
    if not r.status_code == 200:
        print('Error: incorrect api request, HTTP error code',r.status_code)
        exit()

    # check hash code to verify message integrity. From the documentation: HMAC(SHA-256, <dt_value>-<data_value>, <secret key>)
    byte_key = HMAC_SHA_KEY.encode('utf-8')
    msg = json.dumps(r.json()["dt"]) + json.dumps(r.json()["data"], separators=(",",":"))
    h = hmac.new( byte_key, msg.encode('utf-8'), hashlib.sha256 ).hexdigest()
    
    # Error: incorrect checksum -> data are incorrect
    if h != r.json()["checksum"]:
        print('Error: checksums do not match - data should be discarded')
        exit()


    # read out sensor data:
    # r.json()["data"] contains a number of data points given in r.json()["total"] 
    # The first data point is the most recent.
    # the data measurements are usually exactly 3600 seconds apart (1 hour). 
    # However, sometimes there is a little delay or the measurement 
    # arrives a little sooner than expected.

    num_of_msg = int(len(r.json()["data"]))
    # Read timestamp from file, and write new timestamp to file (only if new messages are found)
    if num_of_msg > 0:
        tgvfunctions.timestamptofile('timefile'+str(sensor), int(r.json()["data"][0][0]))
    for i in range(num_of_msg): 
        timestamp = int(r.json()["data"][i][0])*1000 #ms since epoch
        value = {
            "project_id": "soil_moisture",
            "project_description": project_description,
            "application_id": "flowfacts",
            "device_id": r.json()["node id"],
            "device_manufacturer": "Aquaflow",
            "device_description": r.json()["node name"],
            "device_serial": r.json()["node serial"],
            "device_type": r.json()["node model"],
            "timestamp": timestamp,
            "location_description": location_description,
            "latitude": latitude,
            "longitude": longitude,
            "measurements": [
                {
                    "measurement_id": "Electrical Conductivity",
                    "measurement_description": "Electrical conductivity",
                    "value": r.json()["data"][i][1] + ec_offset,
                    "unit": "µS"
                },
                {
                    "measurement_id": "Soil Temperature",
                    "measurement_description": "Soil temperature",
                    "value": r.json()["data"][i][2] + temp_offset,
                    "unit": "°C"
                },
                {
                    "measurement_id": "Volumetric Water Content",
                    "measurement_description": "Soil moisture",
                    "value": r.json()["data"][i][3] + vwc_offset,
                    "unit": "% (m³/m³)"
                }
            ]
        }
        tgvfunctions.produce(producer, value)

    # 3) Read out battery levels
    # Define headers and parameters
    # No need to redefine begintime and endtime here, or to save a timestamp to a textfile.
    headers = {"FFAPIKEY": FFAPIKEY}
    params = {"from": begintime, "to": endtime, "client_id": CLIENT_ID, "node_id": sensor, "node_deveui": node_deveui}
    r = requests.get(
        "https://api.flowfacts.nl/v1/status", headers = headers, params=params
    )

    # CREATE ERROR MESSAGE
    if not r.status_code == 200:
        print('Error: incorrect api request, HTTP error code',r.status_code)
        exit()

    # check hash code to verify message integrity. From the documentation: HMAC(SHA-256, <dt_value>-<data_value>, <secret key>)
    byte_key = HMAC_SHA_KEY.encode('utf-8')
    msg = json.dumps(r.json()["dt"]) + json.dumps(r.json()["data"], separators=(",",":"))
    h = hmac.new( byte_key, msg.encode('utf-8'), hashlib.sha256 ).hexdigest()

    # CREATE ERROR MESSAGE
    if h != r.json()["checksum"]:
        print('Error: checksums do not match - data should be discarded')
        exit()

    # Produce to topic
    num_of_msg = int(len(r.json()["data"]))
    for i in range(num_of_msg): # for i in range(r.json()["total"]-1)
        timestamp = int(r.json()["data"][i][0])*1000 #ms since epoch
        value = {
            "project_id": "soil_moisture",
            "project_description": project_description,
            "application_id": "flowfacts",
            "device_id": r.json()["node"]["id"],
            "device_manufacturer": "Aquaflow",
            "device_description": r.json()["node"]["name"],
            "device_serial": r.json()["node"]["serial"],
            "device_type": r.json()["node"]["model"],
            "timestamp": timestamp,
            "location_description": location_description,
            "latitude": latitude,
            "longitude": longitude,
            "measurements": [
                {
                    "measurement_id": "Battery Voltage",
                    "measurement_description": "Battery Voltage",
                    "value": r.json()["data"][i][1]["bat"],
                    "unit": "mV"
                }
            ]
        }
        tgvfunctions.produce(producer, value)
producer.flush()
