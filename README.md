This producer fetches the sensor data from the Flowfacts API at https://api.flowfacts.nl/v1/...

The sensor data is collected on an hourly basis.

The script reads out the sensor data in 3 parts:
First it reads out the sensor specific information, such as location and measurement offsets, from https://api.flowfacts.nl/v1/nodes.
Thereafter it reads out the actual measurements per sensor at https://api.flowfacts.nl/v1/measurements.
Thirdly it reads out the battery levels per sensor at https://api.flowfacts.nl/v1/status.

## Installation on Virtual Machine

In order to install this script perform the following steps: 

First, clone this project code from GitLab.
Secondly, create a virtual environment, and install the requirements:
```
python -m venv env
source env/bin/activate

pip install -r requirements.txt
```

Then, make the bash script executable
```
sudo chmod u+x flowfacts-producer.sh
```

The Kafka producer runs as a cronjob on the Virtual Machine of The Green Village.

Define a cronjob to read data periodically.
To edit the user's cron file:
```
crontab -e
```

Add this line at the end of the file:
```
1 * * * * ~/producers/flowfacts/flowfacts-producer.sh > ~/log/flowfacts.txt 2>&1
```
This ensures that the code runs at 1 minute past every hour.
